<?php namespace StudioBosco\BackendNotifications\Controllers;

use Input;
use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Backend\Tests\Helpers\BackendHelperTest;
use StudioBosco\BackendNotifications\Helpers\BackendNotifications;

/**
 * Notifications Back-end Controller
 */
class Notifications extends Controller
{
    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController',
    ];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'studiobosco.backendnotifications::manage_notifications',
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('StudioBosco.BackendNotifications', 'backendnotifications', 'notifications');
        $this->addCss('/plugins/studiobosco/backendnotifications/assets/css/backendnotifications.css');
    }

    public function listExtendQuery($query)
    {
        $query->listBackend();
        return $query;
    }

    public function onRead()
    {
        $recordId = Input::get('record_id');

        if (!$recordId) {
            return;
        }

        BackendNotifications::read($recordId);

        return $this->listRefresh();
    }

    public function onReadAll()
    {
        $record = BackendNotifications::readAll();
        return $this->listRefresh();
    }

    public function create()
    {
        if (!$this->user->hasAccess('studiobosco.backendnotifications::create_notifications')) {
            return redirect(Backend::url('studiobosco/backendnotifications/notifications'));
        } else {
            return parent::create();
        }
    }

    public function create_onSave()
    {
        $type = Input::get('Notification.type');

        if ($type === 'global') {
            $subject = Input::get('Notification.subject');
            $body = Input::get('Notification.body');
            $url = Input::get('Notification.url');
            BackendNotifications::globalNotifyAll($subject, $body, $url);

            return redirect(Backend::url('studiobosco/backendnotifications/notifications'));
        } else {
            return parent::create_onSave();
        }
    }

    public function listInjectRowClass($record, $definition)
    {
        return $record->type ? 'is-' . $record->type : 'is-direct';
    }
}
