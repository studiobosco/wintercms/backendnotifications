<?php namespace StudioBosco\BackendNotifications\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class V1_1_0 extends Migration
{
    public function up()
    {
        Schema::table('studiobosco_backendnotifications_notifications', function (Blueprint $table) {
            $table->string('type')->default('direct')->nullable();
        });
    }

    public function down()
    {
        Schema::table('studiobosco_backendnotifications_notifications', function (Blueprint $table) {
            $table->dropColumn([
                'type',
            ]);
        });
    }
}
