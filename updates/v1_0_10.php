<?php namespace StudioBosco\BackendNotifications\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class V1_0_10 extends Migration
{
    public function up()
    {
        Schema::table('studiobosco_backendnotifications_notifications', function (Blueprint $table) {
            $table->index('user_id');
        });
    }

    public function down()
    {
        Schema::table('studiobosco_backendnotifications_notifications', function (Blueprint $table) {
            $table->dropIndex([
                'user_id',
            ]);
        });
    }
}
