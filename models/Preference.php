<?php namespace StudioBosco\BackendNotifications\Models;

use App;
use Lang;
use Model;
use Config;
use Session;
use BackendAuth;
use DirectoryIterator;
use DateTime;
use DateTimeZone;

/**
 * Backend preferences for the backend user
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Preference extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Behaviors implemented by this model.
     */
    public $implement = [
        \Backend\Behaviors\UserPreferencesModel::class
    ];

    /**
     * @var string Unique code
     */
    public $settingsCode = 'studiobosco.backendnotifications::notifications.preferences';

    /**
     * @var mixed Settings form field defitions
     */
    public $settingsFields = 'fields.yaml';

    /**
     * @var array Validation rules
     */
    public $rules = [];

    /**
     * Initialize the seed data for this model. This only executes when the
     * model is first created or reset to default.
     * @return void
     */
    public function initSettingsData()
    {
        $this->enable_notifications = true;
        $this->show_count = true;
        $this->enable_browser_push_notifications = true;
        $this->polling_interval = 5000;
    }

    public function getEnableNotificationsAttribute($value)
    {
        return boolval(intval($value));
    }

    public function getShowCountAttribute($value)
    {
        return boolval(intval($value));
    }

    public function getEnableBrowserPushNotificationsAttribute($value)
    {
        return boolval(intval($value));
    }

    public function getPollingIntervalOptions()
    {
        $options = [
            '1000' => Lang::get('studiobosco.backendnotifications::lang.polling_interval_options.1000'),
            '5000' => Lang::get('studiobosco.backendnotifications::lang.polling_interval_options.5000'),
            '10000' => Lang::get('studiobosco.backendnotifications::lang.polling_interval_options.10000'),
            '30000' => Lang::get('studiobosco.backendnotifications::lang.polling_interval_options.30000'),
            '60000' => Lang::get('studiobosco.backendnotifications::lang.polling_interval_options.60000'),
            '300000' => Lang::get('studiobosco.backendnotifications::lang.polling_interval_options.300000'),
        ];

        return $options;
    }
}
