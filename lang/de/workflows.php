<?php return [
    'nodes' => [
        'notify' => [
            'title' => 'Backend-Benachrichtigung',
            'description' => 'Sendet eine Backend-Benachrichtigung',
        ],
    ],
    'inputs' => [
        'user_id' => 'Nutzer ID',
        'user' => 'Nutzer',
        'subject' => 'Betreff',
        'body' => 'Text',
        'url' => 'URL',
    ],
];
